import thunk from 'redux-thunk'
import { Dispatch } from 'redux'
import {store} from '../index'

import { Iresponse, 
    Iproducts, 
    LOAD_DATA, 
    FETCH_DATA,
    ADD_TO_WISHLIST, 
    REMOVE_FROM_WISHLIST, 
    LoadDataAction, 
    AddToWishlistAction, 
    RemoveFromWishlistAction, 
    fetchDataAction} from '../types/types.d'

export function asyncApiCall(){
    console.log("call")
    async function loadData(){
        var results = await fetch('http://localhost:5000/products')
        var data = await results.json()
        // console.log(data)      
        return data  
    }

    loadData()
    .then(data => store.dispatch(fetchData(data["default"])))
}

export const fetchData = (data : Iresponse) :fetchDataAction =>   { 
    console.log(data)   
    return {
        type: FETCH_DATA,
        payload: data.response.products
    }
}


export function addToWishlist(product : string) : AddToWishlistAction{
    return{
        type: ADD_TO_WISHLIST,
        payload: product
    }
}

export function removeFromWishlist(product : string) : RemoveFromWishlistAction{
    return {
        type : REMOVE_FROM_WISHLIST,
        payload: product
    }
}