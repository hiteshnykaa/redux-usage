import React from 'react';
import {Component} from 'react';
import './styles/App.css';
import {ThunkAction} from 'redux-thunk'
import rootReducer from '../reducers/rootReducer'
import { store } from '../index'

import {IsystemState} from '../types/system-state/types'
import { fetchData, addToWishlist, removeFromWishlist} from '../actions/actions'

interface AppProps {
    state : IsystemState,
    addToWishlist: typeof addToWishlist,
    removeFromWishlist: typeof removeFromWishlist
}

class App extends Component{
  
  receiveData=()=>{
    console.log(store.getState())
  }

  addABC = () => {
    store.dispatch(addToWishlist("abc"))
    console.log(store.getState())
  }

  removeABC =() => {
    store.dispatch(removeFromWishlist("abc"))
    console.log(store.getState())
  }

  render = () => {
    return(
      <div>
        <div onClick={this.receiveData}>Display data</div>
        <div onClick={this.addABC}>Add abc to wishlist</div>
        <div onClick={this.removeABC}>Remove abc from wishlist</div>
      </div>
    )
  }
}

export default App;
