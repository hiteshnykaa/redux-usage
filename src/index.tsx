import React from 'react';
import ReactDOM from 'react-dom';
import './components/styles/index.css';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware,compose} from 'redux';
import {Provider} from 'react-redux'
import rootReducer from './reducers/rootReducer'
import thunk from 'redux-thunk'
import { fetchData, addToWishlist, asyncApiCall } from './actions/actions';
import { composeWithDevTools } from 'redux-devtools-extension';

export const store = createStore(rootReducer, compose(
    applyMiddleware(thunk),
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
  ));
  


ReactDOM.render(
    <Provider store = {store}>
        <App />
    </Provider>
    , document.getElementById('root'));


asyncApiCall();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
