export const FETCH_DATA = 'FETCH_DATA'
export const LOAD_DATA = 'LOAD_DATA'
export const ADD_TO_WISHLIST = 'ADD_TO_WISHLIST'
export const REMOVE_FROM_WISHLIST = 'REMOVE_FROM_WISHLIST'

interface fetchDataAction{
    type: typeof FETCH_DATA
    payload : Iproducts[]
}

interface LoadDataAction {
    type: typeof LOAD_DATA
    payload: Iresponse
}

interface AddToWishlistAction {
    type: typeof ADD_TO_WISHLIST
    payload: string
}

interface RemoveFromWishlistAction {
    type : typeof REMOVE_FROM_WISHLIST
    payload: string
}

export interface Iproducts {
    id: string,
    sku: string,
    title: string,
    subTitle: string,
    price: number,
    discountedPrice: number, 
    type: string,
    isOutOfStock: number,
    imageUrl: string,
    categoryId: string []  
}

export interface Iresponse {
    status : string,
    response : {
        title : string,
        count : number,
        products : Iproducts[],
        filters: Ifilters[]
    }   
}

export interface Ifilters{
    key : string,
    title : string,
    values ?: Value[],
    range ?: Range
}

interface Value {
    name : string,
    id: string
}

interface Range{
    min: number,
    max : number
}
