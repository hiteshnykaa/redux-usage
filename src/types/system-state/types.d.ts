import { Iproducts, Iresponse } from '../types';

export interface IsystemState {
    loading : boolean,
    addedProducts : string[],
    apiResponse : Iproducts[]
}

