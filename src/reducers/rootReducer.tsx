import {IsystemState} from '../types/system-state/types';
import {LOAD_DATA, 
    ADD_TO_WISHLIST, 
    REMOVE_FROM_WISHLIST, 
    LoadDataAction,
    fetchDataAction,
    AddToWishlistAction,
    RemoveFromWishlistAction,
    FETCH_DATA,
    Iproducts,
    } from '../types/types.d'

const initialState : IsystemState = {
    loading: true,
    addedProducts : [],
    apiResponse : [] as Iproducts[]
}

// export default function reducer(){};

export default function rootReducer(
    state = initialState,
    action : fetchDataAction  |   RemoveFromWishlistAction |     AddToWishlistAction, 
) : IsystemState{
   switch (action.type){
        case FETCH_DATA:
            console.log(action.payload)
            return {
                ...state,
                loading: false,
                apiResponse : action.payload
            }
        case ADD_TO_WISHLIST:
            return {
                ...state,
                addedProducts : [...state.addedProducts, action.payload]
            }
        case REMOVE_FROM_WISHLIST:
            return {
                ...state,
                loading : false,
                addedProducts : state.addedProducts.filter(
                    value => value !== action.payload
                )
            }
            default:
                return state
   } 
}